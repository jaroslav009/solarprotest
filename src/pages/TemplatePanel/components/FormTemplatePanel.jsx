import React from 'react'
import BodyText from 'src/components/BodyText'
import {
  RadioGroup,
  Radio,
  Button,
  FormControlLabel,
  TextField,
} from '@mui/material'
import AccordionCustom from 'src/components/AccordionCustom';

const FormTemplatePanel = () => {
  return (
    <div className="fill pb-40">
      <BodyText
        className="title-page"
        containerClassName="title-page-container"
      >
       פריסת פנלים
      </BodyText>
      <Button
          variant="contained"
          className="btn-submit"
      >
       הוספת שורה        
      </Button>
      <div className="mt-13">
        <AccordionCustom label="שורה 1">
          <RadioGroup
            aria-labelledby="radio-2"
            defaultValue="1"
            name="radio-buttons-group"
          >
            <FormControlLabel value="1" control={<Radio />} label="1 פנל בעמידה" className="container-radio" />
            <FormControlLabel value="2" control={<Radio />} label="1 פנל בעמידה ו-1 בשכיבה" className="container-radio" />
            <FormControlLabel value="3" control={<Radio />} label="2 פנלים בעמידה" className="container-radio" />
            <FormControlLabel value="4" control={<Radio />} label="3 פנלים בעמידה" className="container-radio" />
            <FormControlLabel value="5" control={<Radio />} label="2 פנל בעמידה ו-1 בשכיבה" className="container-radio" />
          </RadioGroup>
        </AccordionCustom>
        <AccordionCustom label="שורה 2">
          <RadioGroup
            aria-labelledby="radio-2"
            defaultValue="1"
            name="radio-buttons-group"
          >
            <FormControlLabel value="1" control={<Radio />} label="1 פנל בעמידה" className="container-radio" />
            <FormControlLabel value="2" control={<Radio />} label="1 פנל בעמידה ו-1 בשכיבה" className="container-radio" />
            <FormControlLabel value="3" control={<Radio />} label="2 פנלים בעמידה" className="container-radio" />
            <FormControlLabel value="4" control={<Radio />} label="3 פנלים בעמידה" className="container-radio" />
            <FormControlLabel value="5" control={<Radio />} label="2 פנל בעמידה ו-1 בשכיבה" className="container-radio" />
          </RadioGroup>
        </AccordionCustom>
        <AccordionCustom label="שורה 3">
          <RadioGroup
            aria-labelledby="radio-2"
            defaultValue="1"
            name="radio-buttons-group"
          >
            <FormControlLabel value="1" control={<Radio />} label="1 פנל בעמידה" className="container-radio" />
            <FormControlLabel value="2" control={<Radio />} label="1 פנל בעמידה ו-1 בשכיבה" className="container-radio" />
            <FormControlLabel value="3" control={<Radio />} label="2 פנלים בעמידה" className="container-radio" />
            <FormControlLabel value="4" control={<Radio />} label="3 פנלים בעמידה" className="container-radio" />
            <FormControlLabel value="5" control={<Radio />} label="2 פנל בעמידה ו-1 בשכיבה" className="container-radio" />
          </RadioGroup>
        </AccordionCustom>
        <AccordionCustom label="שורה 4">
          <RadioGroup
            aria-labelledby="radio-2"
            defaultValue="1"
            name="radio-buttons-group"
          >
            <FormControlLabel value="1" control={<Radio />} label="1 פנל בעמידה" className="container-radio" />
            <FormControlLabel value="2" control={<Radio />} label="1 פנל בעמידה ו-1 בשכיבה" className="container-radio" />
            <FormControlLabel value="3" control={<Radio />} label="2 פנלים בעמידה" className="container-radio" />
            <FormControlLabel value="4" control={<Radio />} label="3 פנלים בעמידה" className="container-radio" />
            <FormControlLabel value="5" control={<Radio />} label="2 פנל בעמידה ו-1 בשכיבה" className="container-radio" />
          </RadioGroup>
        </AccordionCustom>
      </div>
      <div className="separate-block mt-13">
        <div className="rowFill">
          <BodyText>כמות פנלים</BodyText>
          <TextField placeholder="3" className="sm-input" />
        </div>
        <div className="rowFill mt-6">
          <BodyText>זווית פנלים/זווית המשלושים</BodyText>
          <TextField placeholder="18" className="sm-input" />
        </div>
        <div className="rowFill mt-6">
          <BodyText>גובה רגל קדמית</BodyText>
          <TextField placeholder={'100 מ"מ'} className="sm-input" />
        </div>
      </div>
      <div className="rowStart alignCenter mt-21">
        <div>
          <BodyText className="change-position-text ml-27">לשינוי מיקום השורה</BodyText>
        </div>
        <Button variant="contained">לחץ כאן וגרור את השורה</Button>
      </div>
      <BodyText className="change-position-text" containerClassName="mt-6">הזזת שורה מדוייקת</BodyText>
      <div className="rowFill mt-15 column-middle">
        <div className="rowFill">
          <div className="rowStart alignCenter ml-11 ml-middle">
            <BodyText className="label-sm-inp" containerClassName="ml-8">ימינה</BodyText>
            <TextField label="" className="sm-input" />
          </div>
          <div className="rowStart alignCenter ml-11 ml-middle">
            <BodyText className="label-sm-inp" containerClassName="ml-8">ימינה</BodyText>
            <TextField label="" className="sm-input" />
          </div>
        </div>
        <div className="rowFill">
          <div className="rowStart alignCenter ml-11 ml-middle">
            <BodyText className="label-sm-inp" containerClassName="ml-8">ימינה</BodyText>
            <TextField label="" className="sm-input" />
          </div>
          <div className="rowStart alignCenter ml-middle">
            <BodyText className="label-sm-inp" containerClassName="ml-8">ימינה</BodyText>
            <TextField label="" className="sm-input" />
          </div>
        </div>
      </div>
      
    </div>
  )
}

export default FormTemplatePanel
