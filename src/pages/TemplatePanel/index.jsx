import React from 'react'
import { useNavigate } from 'react-router-dom'
import Layout from 'src/components/Layout'
import FormTemplatePanel from './components/FormTemplatePanel'
import './styles.scss'

const TemplatePanelScreen = () => {
  const navigate = useNavigate()
  return (
    <Layout
      header={{ proggressHeader: { indexComplete: 5, currentStep: 6, onClickItem: () => navigate('/') } }}
      className="layout-custom-panel"
    >
        <FormTemplatePanel />
    </Layout>
  )
}

export default TemplatePanelScreen
