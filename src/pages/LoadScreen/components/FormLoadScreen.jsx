/* eslint-disable quotes */
/* eslint-disable react/no-unescaped-entities */
import React from 'react'
import TextField from '@mui/material/TextField'
import BodyText from 'src/components/BodyText'
import {
  FormControl,
  RadioGroup,
  Radio,
  Tooltip,
  MenuItem,
  Select,
  Button,
} from '@mui/material'
import InfoIcon from '@mui/icons-material/Info'
import { buildingList, territoryList } from '../data'
import RadioBlock from 'src/components/RadioBlock'

const FormLoadScreen = () => {
  return (
    <div className="fill">
      <BodyText
        className="title-page"
        containerClassName="title-page-container"
      >
        עומסים
      </BodyText>
      <TextField
        id="outlined-basic"
        label="עומסי רוח"
        variant="outlined"
        placeholder="נתון שמגיע ממפה - לא ניתן לשינוי"
      />
      <FormControl style={{ marginTop: 25 }}>
        <RadioGroup
          aria-labelledby="radio-1"
          defaultValue="1"
          name="radio-buttons-group"
        >
          <div className="rowStart">
            {territoryList.map((value, index) => {
              return (
                <div
                  className={`${index !== 0 ? 'mr-20' : 'mr-0'}`}
                  key={value.label}
                >
                  <RadioBlock
                    value={value.label}
                    label={value.label}
                  />
                </div>
              )
            })}
          </div>
        </RadioGroup>
      </FormControl>
      <BodyText className="title-label" containerClassName="title-label-container">
        עומסי שלג - לא ניתן לשינוי
      </BodyText>
      <BodyText containerClassName="mt-5" className="fns-18">
        3 מטרים בשנה
      </BodyText>
      <BodyText containerClassName="title-label-container" className="title-label fns-18">
        משקולות
      </BodyText>
      <BodyText className="title-label" containerClassName="mt-5">
        עומס בק"ג למטר מ"ר פנל
      </BodyText>
      <FormControl>
        <RadioGroup
          aria-labelledby="radio-2"
          defaultValue="1"
          name="radio-buttons-group"
        >
          <div className="rowStart" style={{ marginTop: 14 }}>
            <div className="rowStart fill">
              <Radio
                value="1"
              />
              <TextField
                id="outlined-basic"
                label=""
                variant="outlined"
                className="fill"
                placeholder="נתון מיובא על ידי סולאר פרו"
              />
            </div>
            <Tooltip
              title="יופיע הסבר - באחריות לידור"
              placement="top"
              className="cursor"
              style={{ marginRight: 10 }}
            >
              <InfoIcon />
            </Tooltip>
          </div>
          <div className="rowStart" style={{ marginTop: 23 }}>
            <div className="rowStart fill">
              <Radio
                value="2"
              />
              <TextField
                id="outlined-basic"
                label=""
                variant="outlined"
                className="fill"
                placeholder={`הזנה ידנית (מינ' 40 ק"ג למ"ר פנל)`}
              />
            </div>
            <Tooltip
              title="יופיע הסבר - באחריות לידור"
              placement="top"
              className="cursor"
              style={{ marginRight: 10, opacity: 0 }}
            >
              <InfoIcon />
            </Tooltip>
          </div>
        </RadioGroup>
      </FormControl>
      <BodyText className="title" containerClassName="title-container">
        קונסטרוקציה
      </BodyText>
      <div className="w100 rowStart">
        <Select
          labelId="select-build"
          id="select-build-id"
          className="fill ml-34 pr-34"
        >
          {buildingList.map((val) => {
            return (
              <MenuItem value={val.value} key={val.label}>
                {val.label}
              </MenuItem>
            )
          })}
        </Select>
      </div>
      <div className="center" style={{ marginTop: 40 }}>
        <Button
          variant="contained"
          className="btn-submit"
        >
          המשך
        </Button>
      </div>
    </div>
  )
}

export default FormLoadScreen
