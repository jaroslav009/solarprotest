import React, { useCallback } from 'react'
import { useNavigate } from 'react-router-dom'
import Layout from 'src/components/Layout'
import FormLoadScreen from './components/FormLoadScreen'
import './styles.scss'

const LoadScreen = () => {
  const navigate = useNavigate()
  return (
    <Layout
      header={{ proggressHeader: { indexComplete: 6, currentStep: 7, onClickItem: () => navigate('/template-panel') } }}
      className="layout-custom-load"
    >
      <FormLoadScreen />
    </Layout>
  )
}

export default LoadScreen
