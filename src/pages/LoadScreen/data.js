export const buildingList = [
  {
    label: '42 ק"ג',
    value: 42,
  },
  {
    label: '50 ק"ג',
    value: 50,
  },
  {
    label: '65 ק"ג',
    value: 65,
  },
  {
    label: '100 ק"ג',
    value: 100,
  },
]

export const territoryList = [
  {
    label: 'איזור פתוח',
  },
  {
    label: 'איזור עירוני',
  },
  {
    label: 'איזור מוגן',
  },
]
