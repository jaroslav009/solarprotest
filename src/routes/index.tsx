import React from 'react'
import { Route, Routes } from 'react-router-dom'
import LoadScreen from 'src/pages/LoadScreen'
import { ROUTES } from './routesData'

const MainRoute = () => {
  return (
    <Routes>
      {ROUTES.map((val) => {
        return <Route {...val} />
      })}
    </Routes>
  )
}

export default MainRoute
