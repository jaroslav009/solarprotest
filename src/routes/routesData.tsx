import React from 'react'
import LoadScreen from 'src/pages/LoadScreen'
import TemplatePanelScreen from 'src/pages/TemplatePanel'

export const ROUTES = [
  { path: '/', key: 'ROOT', exact: true, element: <LoadScreen /> },
  { path: '/template-panel', key: 'ROOT2', exact: true, element: <TemplatePanelScreen /> },
]
