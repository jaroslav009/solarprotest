import React from 'react'
import MainRoute from './routes'
import { CacheProvider } from '@emotion/react'
import { createTheme, ThemeProvider } from '@mui/material/styles'
import rtlPlugin from 'stylis-plugin-rtl'
import { prefixer } from 'stylis'
import createCache from '@emotion/cache'

const theme = createTheme({
  direction: 'rtl',
  typography: {
    fontFamily: 'Open Sans Hebrew Regular',
  },
})

const cacheRtl = createCache({
  key: 'muirtl',
  stylisPlugins: [prefixer, rtlPlugin],
})

function App() {
  return (
    <CacheProvider value={cacheRtl}>
      <ThemeProvider theme={theme}>
        <MainRoute />
      </ThemeProvider>
    </CacheProvider>
  )
}

export default App
