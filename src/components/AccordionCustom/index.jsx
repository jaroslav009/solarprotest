import React from 'react'
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import ContentCopyIcon from '@mui/icons-material/ContentCopy'
import BodyText from '../BodyText';

const AccordionCustom = ({ children, label }) => {
  return (
    <Accordion>
        <AccordionSummary
          expandIcon={
              <div className="rowStart rotate-container">
                  <DeleteForeverIcon sx={{ width: 17 }} />
                  <ContentCopyIcon sx={{ width: 17, marginRight: 0.5, marginLeft: 0.5, }} />
                  <div className="rotate-expand">
                    <ExpandMoreIcon />
                  </div>
              </div>
          }
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <BodyText className="label-accordion">{label}</BodyText>
        </AccordionSummary>
      <AccordionDetails>
            {children}
      </AccordionDetails>
    </Accordion>
  )
}

export default AccordionCustom
