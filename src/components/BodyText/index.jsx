import React from 'react'
import { fontWeightGet } from './font-weight'
import 'src/styles/fonts.scss'
import './styles.scss'

const BodyText = ({
  children,
  fontWeight,
  fontSize,
  color = '#373737',
  style,
  lineHeight,
  center,
  isBlock = true,
  styleContainer,
  containerClassName = '',
  marginLeft,
  marginRight,
  marginBottom,
  marginTop,
  numberOfLines,
  className,
  ...restProps
}) => {
  const font = fontWeightGet(fontWeight || '400')
  if (isBlock) {
    return (
      <div
        style={{
          marginRight,
          marginLeft,
          marginBottom,
          marginTop,
          ...styleContainer,
          textAlign: center ? 'center' : 'initial',
        }}
        className={containerClassName}
      >
        <span
          style={{
            ...fontWeight ? font : {},
            fontSize,
            color,
            lineHeight,
            textAlign: center ? 'center' : 'initial',
            whiteSpace: 'break-spaces',
            WebkitLineClamp: numberOfLines || 'initial',
            ...style,
          }}
          className={`${numberOfLines ? 'numberOfLines' : ''} ${className}`}
          {...restProps}
        >
          {children}
        </span>
      </div>
    )
  }
  return (
    <span
      style={{
        ...font,
        fontSize,
        color,
        lineHeight,
        textAlign: center ? 'center' : 'initial',
        marginRight,
        marginLeft,
        marginBottom,
        marginTop,
        whiteSpace: 'break-spaces',
        WebkitLineClamp: numberOfLines || 'initial',
        ...style,
      }}
      className={`${numberOfLines ? 'numberOfLines' : ''} ${className}`}
      {...restProps}
    >
      {children}
    </span>
  )
}

export default BodyText
