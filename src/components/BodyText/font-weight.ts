export const fontWeightGet = (fontWeight: string | number) => {
  switch (fontWeight) {
    case '400':
      return {
        fontFamily: 'Open Sans Hebrew Regular',
        fontWeight: 400,
      }
    case '700':
      return {
        fontFamily: 'Open Sans Hebrew Bold',
        fontWeight: 'bold',
      }
    default:
      return {
        fontFamily: 'SF Pro Display Regular',
        fontWeight: 400,
      }
  }
}
