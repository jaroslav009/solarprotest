import React from 'react'
import BodyText from '../BodyText'
import './styles.scss'

const RadioBlock = ({ label, isActive, onClick }) => {
  return (
    <div className="cursor" onClick={onClick}>
      <div
        className={`block-radio ${isActive ? 'block-radio-active' : ''}`}
      ></div>
      <BodyText className="label-text" center>
        {label}
      </BodyText>
    </div>
  )
}

export default RadioBlock
