export const dataProggress = [
  {
    label: 'פתיחת פרויקט',
  },
  {
    label: 'סוג הגג',
  },
  {
    label: 'תכנון',
  },
  {
    label: 'מכשולים',
  },
  {
    label: 'פנלים',
  },
  {
    label: 'נקודת חשמל',
  },
  {
    label: 'עומסים',
  },
  {
    label: 'קבלת מפרט',
  },
]
