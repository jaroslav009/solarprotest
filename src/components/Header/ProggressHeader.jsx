import React from 'react'
import BodyText from '../BodyText'
import CheckIcon from '@mui/icons-material/Check'

import { dataProggress } from './dataProggress'

const ProggressHeader = ({
  indexComplete,
  currentStep,
  onClickItem,
}) => {
  return (
    <div className="header-container-proggress">
      <div className="header-proggress-line">
        <div className="rowFill list-circle">
          {dataProggress.map((val, index) => {
            const isComplete = index + 1 <= (indexComplete || 0)
            const isCurrentStep = index + 1 === (currentStep || 0)
            const isFirstLast =
              index === 0 || index === dataProggress.length - 1
            return (
              <div
                key={val.label}
                className="item-proggress cursor"
                style={
                  isFirstLast
                    ? {
                        alignItems: index === 0 ? 'flex-start' : 'flex-end',
                      }
                    : {}
                }
                onClick={() => onClickItem(val, index)}
              >
                <div
                  className={
                    isCurrentStep
                      ? 'circle-current-step'
                      : `circle-proggress ${
                          isComplete ? 'circle-proggress-complete' : ''
                        }`
                  }
                >
                  {!isCurrentStep && isComplete && (
                    <CheckIcon
                      fill="#fff"
                      sx={{ color: '#fff', width: 10, height: 10 }}
                    />
                  )}
                </div>
                <BodyText
                  styleContainer={
                    isFirstLast
                      ? {
                          marginRight: index === 0 ? -30 : 0,
                          marginLeft: index !== 0 ? -30 : 0,
                        }
                      : {}
                  }
                  color={isCurrentStep ? '#373737' : '#D5D5D5'}
                  className="proggress-header-title"
                >
                  {val.label}
                </BodyText>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}

export default ProggressHeader
