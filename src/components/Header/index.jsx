import React from 'react'
import 'src/styles/global.scss'
import ProggressHeader from './ProggressHeader'
import './styles.scss'
import '../../styles/fonts.scss'

const Header = ({ proggressHeader }) => {
  return (
    <div className="rowFill header-container">
      <div className="header-element"></div>
      <ProggressHeader {...proggressHeader} />
      <div className="header-element"></div>
    </div>
  )
}

export default Header
