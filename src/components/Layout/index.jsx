import React from 'react'
import Header from '../Header'
import './styles.scss'

const Layout = ({ header, className, children }) => {
  return (
    <div className="fill">
      <Header {...header} />
      <div className="rowStart fill">
        {/* Custom area, for a form mostly */}
        <div className={`layout-content-container ${className || ''}`}>
          {children}
        </div>
        {/* Map area */}
        <div className="layout-map-container">
          <iframe
            scrolling="no"
            src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=1%20Grafton%20Street,%20Dublin,%20Ireland+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
            style={{ width: '100%', height: '100%' }}
          >
            <a href="https://www.gps.ie/marine-gps/">ship tracker</a>
          </iframe>
        </div>
      </div>
    </div>
  )
}

export default Layout
